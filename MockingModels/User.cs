﻿using System;

namespace MockingModels
{
    public class User
    {
        public string Name { get; set; }
        public Guid Id { get; private set; }

        public User()
        {
            Id = Guid.NewGuid();
        }

        public User(Guid id)
        {
            Id = id;
        }
    }
}
