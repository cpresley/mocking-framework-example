﻿using System;

namespace MockingModels
{
    public class Issue
    {
        public Guid Id { get; private set; }
        public string Description { get; set; }
        public IssueType Type { get; set; }
        public string Title { get; set; }
        public User AssignedUser { get; set; }
        public User ReportingUser { get; set; }

        public Issue()
        {
            Id = Guid.NewGuid();
        }

        public Issue(Guid guid)
        {
            Id = guid;
        }
    }
}
