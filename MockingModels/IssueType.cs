﻿namespace MockingModels
{
    public enum IssueType
    {
        Bug,
        Feature,
        Task,
    }
}
