﻿using MockingServices.Interfaces;
using MockingServices.Repositories;
using NSubstitute;
using NUnit.Framework;
using System;

namespace MockingServices.Tests.Repositories.IssueRepositoryTests
{
    [TestFixture]
    class When_creating_the_repository
    {
        [Test]
        public void And_the_db_access_is_null_then_an_exception_is_thrown()
        {
            Assert.Throws<ArgumentNullException>(() => new IssueRepository(null));
        }

        [Test]
        public void Then_the_repository_is_created()
        {
            Assert.DoesNotThrow(() => new IssueRepository(Substitute.For<IIssueDatabaseAccess>()));
        }
    }
}
