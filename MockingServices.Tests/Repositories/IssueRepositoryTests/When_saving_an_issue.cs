﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MockingModels;
using MockingServices.Interfaces;
using MockingServices.Repositories;
using NSubstitute;
using NUnit.Framework;

namespace MockingServices.Tests.Repositories.IssueRepositoryTests
{
    [TestFixture]
    class When_saving_an_issue
    {
        private IIssueDatabaseAccess _dbAccess;
        private IssueRepository _repostiory;

        [SetUp]
        public void Setup()
        {
            _dbAccess = Substitute.For<IIssueDatabaseAccess>();
            _repostiory = new IssueRepository(_dbAccess);
        }

        [Test]
        public void And_the_issue_is_null_then_an_exception_is_thrown()
        {
            Assert.Throws<ArgumentNullException>(() => _repostiory.Save(null));
        }

        [Test]
        public void And_the_issue_does_not_exist_then_the_db_access_create_is_called()
        {
            var issue = new Issue();
            _dbAccess.Read(issue.Id).Returns((Issue)null);

            _repostiory.Save(issue);

            _dbAccess.Received(1).Create(issue);
        }

        [Test]
        public void And_the_issue_exists_then_the_db_access_update_is_called()
        {
            var issue = new Issue();
            _dbAccess.Read(issue.Id).Returns(issue);

            _repostiory.Save(issue);

            _dbAccess.Received(1).Update(issue);
        }
    }
}
