﻿using System;
using MockingModels;
using MockingServices.Interfaces;
using MockingServices.Repositories;
using NSubstitute;
using NUnit.Framework;

namespace MockingServices.Tests.Repositories.IssueRepositoryTests
{
    [TestFixture]
    class When_deleting_an_issue
    {
        private IIssueDatabaseAccess _dbAccess;
        private IssueRepository _repository;

        [SetUp]
        public void Setup()
        {
            _dbAccess = Substitute.For<IIssueDatabaseAccess>();
            _repository = new IssueRepository(_dbAccess);
        }

        [Test]
        public void And_the_issue_is_null_then_an_exception_is_thrown()
        {
            Assert.Throws<ArgumentNullException>(() => _repository.Delete(null));
        }

        [Test]
        public void Then_the_db_access_is_used_to_delete_the_issue()
        {
            var issue = new Issue();

            _repository.Delete(issue);

            _dbAccess.Received(1).Delete(issue);
        }
    }
}
