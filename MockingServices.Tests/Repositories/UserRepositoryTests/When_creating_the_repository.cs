﻿using System;
using MockingServices.Interfaces;
using MockingServices.Repositories;
using NSubstitute;
using NUnit.Framework;

namespace MockingServices.Tests.Repositories.UserRepositoryTests
{
    [TestFixture]
    // ReSharper disable InconsistentNaming
    public class When_creating_the_repository
    {
        [Test]
        public void And_the_database_access_is_null_then_an_exception_is_thrown()
        {
            Assert.Throws<ArgumentNullException>(() => new UserRepository(null));
        }

        [Test]
        public void Then_the_repository_is_created()
        {
            Assert.DoesNotThrow(() => new UserRepository(Substitute.For<IUserDatabaseAccess>()));
        }
    }

    // ReSharper restore InconsistentNaming
}
