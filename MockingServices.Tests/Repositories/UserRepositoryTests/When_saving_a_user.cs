﻿using System;
using MockingModels;
using MockingServices.Interfaces;
using MockingServices.Repositories;
using NSubstitute;
using NUnit.Framework;

namespace MockingServices.Tests.Repositories.UserRepositoryTests
{
    [TestFixture]
    // ReSharper disable InconsistentNaming
    class When_saving_a_user
    {
        private IUserDatabaseAccess _dbAccess;
        private UserRepository _repository;

        [SetUp]
        public void Setup()
        {
            _dbAccess = Substitute.For<IUserDatabaseAccess>();
            _repository = new UserRepository(_dbAccess);
        }

        [Test]
        public void And_the_user_is_null_then_an_exception_is_thrown()
        {
            Assert.Throws<ArgumentNullException>(() => _repository.Save(null));
        }

        [Test]
        public void And_the_user_already_exists_then_the_db_access_calls_update()
        {
            var user = new User();
            _dbAccess.Read(user.Id).Returns(user);

            _repository.Save(user);

            _dbAccess.Received(1).Update(user);
        }

        [Test]
        public void And_the_user_doesnt_exist_then_the_db_access_calls_create()
        {
            var user = new User();
            _dbAccess.Read(user.Id).Returns(ignored => null);

            _repository.Save(user);

            _dbAccess.Received(1).Create(user);
        }
    }

    // ReSharper restore InconsistentNaming
}
