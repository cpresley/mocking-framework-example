﻿using System;
using MockingModels;
using MockingServices.Interfaces;
using MockingServices.Repositories;
using NSubstitute;
using NUnit.Framework;

namespace MockingServices.Tests.Repositories.UserRepositoryTests
{
    [TestFixture]
    // ReSharper disable InconsistentNaming
    class When_deleting_a_user
    {
        private IUserDatabaseAccess _dbAccess;
        private UserRepository _repository;

        [SetUp]
        public void Setup()
        {
            _dbAccess = Substitute.For<IUserDatabaseAccess>();
            _repository = new UserRepository(_dbAccess);
        }

        [Test]
        public void And_the_user_is_null_then_an_exception_is_thrown()
        {
            Assert.Throws<ArgumentNullException>(() => _repository.Delete(null));
        }

        [Test]
        public void Then_the_database_access_is_called_to_delete_the_user()
        {
            var user = new User();

            _repository.Delete(user);

            _dbAccess.Received(1).Delete(user);
        }
    }

    // ReSharper restore InconsistentNaming
}
