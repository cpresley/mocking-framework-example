﻿using System;
using System.Data.SqlClient;
using MockingModels;
using MockingServices.Interfaces;

namespace MockingServices.DatabaseAccess
{
    public class UserDatabaseAccess : IUserDatabaseAccess
    {
        public void Create(User user)
        {
            if (user == null) throw new ArgumentNullException(nameof(user));

            var query = $"INSERT INTO dbo.Users (Id, Name) VALUES ('{user.Id}', '{user.Name}')";
            var connection = new SqlConnection();
            connection.Open();
            var command = new SqlCommand(query, connection);
            command.ExecuteNonQuery();
            connection.Close();
        }

        public User Read(Guid guid)
        {
            var query = $"SELECT Id, Name FROM dbo.Users WHERE Id='{guid}'";

            var connection = new SqlConnection();
            connection.Open();
            var command = new SqlCommand(query, connection);
            using (var reader = command.ExecuteReader())
            {
                if (!reader.HasRows)
                    return null;
                reader.Read();
                var user = new User(Guid.Parse(reader[reader.GetOrdinal("Id")].ToString()))
                {
                    Name = reader[reader.GetOrdinal("Name")].ToString(),
                };
                reader.Close();
                return user;
            }
        }

        public void Update(User user)
        {
            if (user == null) throw new ArgumentNullException(nameof(user));

            var query = $"UPDATE dbo.Users SET Id='{user.Id}', Name='{user.Name}' WHERE ID = '{user.Id}'";
            var connection = new SqlConnection();
            connection.Open();
            var command = new SqlCommand(query, connection);
            command.ExecuteNonQuery();
            connection.Close();
        }

        public void Delete(User user)
        {
            if (user == null) throw new ArgumentNullException(nameof(user));

            var query = $"DELETE FROM dbo.Users WHERE Id='{user.Id}'";
            var connection = new SqlConnection();
            connection.Open();
            var command = new SqlCommand(query, connection);
            command.ExecuteNonQuery();
            connection.Close();
        }
    }
}
