﻿using MockingModels;
using MockingServices.Interfaces;
using System;
using System.Data.SqlClient;

namespace MockingServices.DatabaseAccess
{
    public class IssueDatabaseAccess : IIssueDatabaseAccess
    {
        private readonly IUserDatabaseAccess _userDbAccess;
        public IssueDatabaseAccess(IUserDatabaseAccess userDbAccess)
        {
            if (userDbAccess == null) throw new ArgumentNullException(nameof(userDbAccess));
            _userDbAccess = userDbAccess;
        }
        public void Create(Issue issue)
        {
            var connection = new SqlConnection();
            connection.Open();

            var query = "INSERT INTO dbo.Issues (Id, Title, Description, Type, AssignedUser, ReportingUser) ";
            query += $"VALUES({issue.Id}, {issue.Title}, {issue.Description}, {issue.Type.ToString()}, {issue.AssignedUser.Id}, {issue.ReportingUser.Id})";

            var command = new SqlCommand(query, connection);
            command.ExecuteNonQuery();
        }

        public Issue Read(Guid id)
        {
            var connection = new SqlConnection();
            connection.Open();

            var query = $"SELECT Id, Title, Description, Type, AssignedUser, ReportingUser FROM dbo.Issues WHERE Id={id}";

            var command = new SqlCommand(query, connection);
            using (var reader = command.ExecuteReader())
            {
                if (!reader.HasRows) return null;

                reader.Read();
                var guid = reader.GetGuid(reader.GetOrdinal("Id"));
                var issue = new Issue(guid)
                {
                    Title = reader.GetString(reader.GetOrdinal("Title")),
                    Description = reader.GetString(reader.GetOrdinal("Description")),
                    Type = (IssueType)Enum.Parse(typeof(IssueType), reader.GetString(reader.GetOrdinal("Type"))),
                    AssignedUser = _userDbAccess.Read(reader.GetGuid(reader.GetOrdinal("AssignedUser"))),
                    ReportingUser = _userDbAccess.Read(reader.GetGuid(reader.GetOrdinal("ReportingUser"))),
                };
                reader.Close();
                return issue;
            }
        }

        public void Update (Issue issue)
        {
            var connection = new SqlConnection();
            connection.Open();

            var query = $"UPDATE dbo.Issues SET Id='{issue.Id}',Title='{issue.Title}', Description='{issue.Description}', Type='{issue.Type.ToString()}', AssignedUser='{issue.AssignedUser.Id}', ReportingUser='{issue.ReportingUser.Id}' ";

            var command = new SqlCommand(query, connection);
            command.ExecuteNonQuery();
        }

        public void Delete (Issue issue)
        {
            var connection = new SqlConnection();
            connection.Open();

            var query = $"Delete FROM dbo.Issues where Id='{issue.Id}'";

            var command = new SqlCommand(query, connection);
            command.ExecuteNonQuery();
        }
    }
}
