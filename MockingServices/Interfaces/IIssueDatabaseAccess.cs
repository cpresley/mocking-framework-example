﻿using System;
using MockingModels;

namespace MockingServices.Interfaces
{
    public interface IIssueDatabaseAccess
    {
        void Create(Issue issue);
        void Delete(Issue issue);
        Issue Read(Guid id);
        void Update(Issue issue);
    }
}