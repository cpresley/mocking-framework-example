using System;
using MockingModels;

namespace MockingServices.Interfaces
{
    public interface IUserDatabaseAccess {
        void Create(User user);
        User Read(Guid guid);
        void Update(User user);
        void Delete(User user);
    }
}