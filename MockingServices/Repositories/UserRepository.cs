﻿using System;
using MockingModels;
using MockingServices.Interfaces;

namespace MockingServices.Repositories
{
    public class UserRepository
    {
        private readonly IUserDatabaseAccess _databaseAccess;

        public UserRepository(IUserDatabaseAccess databaseAccess)
        {
            if (databaseAccess == null) throw new ArgumentNullException(nameof(databaseAccess));
            _databaseAccess = databaseAccess;
        }

        public void Save(User user)
        {
            if (user == null) throw new ArgumentNullException(nameof(user));
            if (_databaseAccess.Read(user.Id) != null)
            {
                _databaseAccess.Update(user);
            }
            else
            {
                _databaseAccess.Create(user);
            }
        }

        public void Delete(User user)
        {
            if (user == null) throw new ArgumentNullException(nameof(user));

            _databaseAccess.Delete(user);
        }
    }
}
