﻿using MockingModels;
using MockingServices.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MockingServices.Repositories
{
    public class IssueRepository
    {
        private readonly IIssueDatabaseAccess _dbAccess;
        public IssueRepository(IIssueDatabaseAccess dbAccess)
        {
            if (dbAccess == null) throw new ArgumentNullException(nameof(dbAccess));

            _dbAccess = dbAccess;
        }

        public void Save (Issue issue)
        {
            if (issue == null) throw new ArgumentNullException(nameof(issue));

            if (_dbAccess.Read(issue.Id) == null)
            {
                _dbAccess.Create(issue);
            }
            else
            {
                _dbAccess.Update(issue);
            }
        }

        public void Delete (Issue issue)
        {
            if (issue == null) throw new ArgumentNullException(nameof(issue));

            _dbAccess.Delete(issue);
        }
    }
}
